import { database } from './firebase';

// User API
export const doManageUser = (id, f, l, a, c) =>
	database.ref(`users/${id}`).set({
		firstName: f,
		lastName: l,
		age: a,
		country: c,
	});

export const getProfile = id =>
	database.ref(`users/${id}`).once('value');

export const getAuthDetails = uid =>
	database.auth().getUser(uid);
/* ==============================Beach API============================================== */

// Read Beaches from database but limit the values for grid
export const getBeaches = limitNumber =>
	database.ref('beaches').limitToFirst(limitNumber).once('value');
// Get beach list for top visisted beaches
export const getBeachPoll = () =>
	database.ref('beaches').orderByChild('visits').limitToLast(7).once('value');

// Get beach list for top rated beaches
export const getTopRated = () =>
	database.ref('beaches').orderByChild('rating').limitToLast(7).once('value');

	// filtering calls

export const FilterBeaches = (key, value) =>
	database.ref('beaches').orderByKey(key).equalTo(value)
		.limitToFirst(9)
		.once('value');

export const VisitBeach = (id, valueToAddOn) =>
	database.ref(`beaches/${id}`).update({
		visits: valueToAddOn + 1,
	});

export const RateBeach = (id, valueToAddOn) =>
	database.ref(`beaches/${id}`).update({
		rating: valueToAddOn + 1,
	});

/* ====================================Post API======================================== */
export const doSubmitpost = (posttext, userID, displayName) =>
	database.ref('posts').push({
		posttext,
		userID,
		displayName,
		date: new Date().toLocaleDateString(),
		likes: 0,
		isDeleted: false,
	});

export const getPosts = () =>
	database.ref('posts').once('value');

export const LikePost = (id, likesInt) =>
	database.ref(`posts/${id}`).update({
		likes: likesInt + 1,
	});

export const doCommentpost = (postid, commentText, userID, displayName) =>
	database.ref('posts').child(postid).child('comments').push({
		commentText,
		userID,
		displayName,
		date: new Date().toLocaleDateString(),
		isDeleted: false,
	});

/* ====================================Messages API======================================== */
export const checkChatExist = (user1, user2) =>
	database.ref('users').child(user1).child('messages').child(user2)
		.once('value');

export const createChat = (user1, user2) =>
	database.ref('userMessages').push({
		user1,
		user2,
	});

export const addChatToIdToBothUsers = (chatID, useruid, user2) =>
	database.ref('users').child(useruid).child('messages').child(user2)
		.set({
			chat: chatID,
		});

export const doPostMessage = (id, messageText, from, to) =>
	database.ref('UserMessages').child(id).push({
		messageText,
		from,
		to,
		date: new Date().toLocaleDateString(),
		isDeleted: false,
	});

export const getMessage = chatID =>
	database.ref('UserMessages').child(chatID)
		.once('value');

export const getUserMessages = user1 =>
	database.ref('users').child(user1).child('messages').once('value');

export const getProfileB = () =>
	database.ref('users').once('value');
