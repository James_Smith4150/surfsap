import * as firebase from 'firebase';

const config = {
	apiKey: 'AIzaSyDpgaqVoAo17AMcie7KZslxe9iJKi7gos8',
	authDomain: 'surfsapp-3aba5.firebaseapp.com',
	databaseURL: 'https://surfsapp-3aba5.firebaseio.com',
	projectId: 'surfsapp-3aba5',
	storageBucket: 'surfsapp-3aba5.appspot.com',
	messagingSenderId: '63330075352',
};

if (!firebase.apps.length) {
	firebase.initializeApp(config);
}

const auth = firebase.auth();
const database = firebase.database();

export {
	auth,
	database,
};
