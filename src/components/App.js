import React, { Component } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import RoutingPage from './RoutingPage';
import withAuthentication from './HOC/withAuthentication';

class App extends Component {
	render() {
		return (
			<Router>
				<Switch>
					<RoutingPage />
				</Switch>
			</Router>
		);
	}
}

export default (withAuthentication(App));
