// React Imports
import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

// Routes
import * as routes from '../constants/routes';

// Page Imports
import LandingPage from './pages/LandingPage';
import DashboardPage from './pages/HomePage';
import ProfilePage from './pages/ProfilePage';
import BeachPage from './pages/BeachPage';
import AboutPage from './pages/AboutPage';
import ChatPage from './pages/ChatPage';
import NotificationPage from './pages/NotificationPage';
import AdminPage from './admin/AdminPage';


// Component Imports
import NavigationAppBar from './shared/navigation/NavigationAppBar';
import HeaderImage from './shared/navigation/HeaderImage';
// Other Imports
//

class RoutingPage extends Component {
	render() {
		return (
			<div>
				{this.props.authUser &&	<HeaderImage />}
				{this.props.authUser &&	<NavigationAppBar />}

				<Route exact path={routes.LANDINGPAGE} component={() => <LandingPage />} />
				<Route exact path={routes.BEACHPAGE} component={() => <BeachPage />} />
				<Route exact path={routes.HOMEPAGE} component={() => <DashboardPage />} />
				<Route exact path={routes.PROFILEPAGE} component={() => <ProfilePage />} />
				<Route exact path={routes.NOTIFICATIONPAGE} component={() => <NotificationPage />} />
				<Route exact path={routes.CHATPAGE} component={() => <AboutPage />} />
				<Route exact path={routes.ABOUTPAGE} component={() => <ChatPage />} />

				<Route exact path={routes.ADMINPAGE} component={() => <AdminPage />} />
			</div>
		);
	}
}

// {this.props.authUser &&	<Footer />}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});

export default connect(mapStateToProps)(RoutingPage);
