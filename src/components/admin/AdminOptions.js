import React, { Component } from 'react';
// import placeholder from '../../images/placeholder.jpg';

class AdminOptions extends Component {
	render() {
		return (
			<div className="row adminOptions" >
				<div className="col-sm-2">
					<button
						onClick={this.props.Messagebtnfunction}
					>
          Manage Users
					</button>
				</div>
				<div className="col-sm-2">
					<button
						onClick={this.props.Messagebtnfunction}
					>
          Manage Beaches
					</button>
				</div>
			</div>
		);
	}
}

// const mapStateToProps = state => ({
// 	authUser: state.sessionState.authUser,
// 	ChatID: state.Message.ChatID,
// 	UserMessages: state.Message.UserMessages,
// });
// const mapDispatchToProps = ({
// 	GetUserMessages,
// });

export default AdminOptions;
