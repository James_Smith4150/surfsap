import React, { Component } from 'react';
import ListCard from '../shared/cards/ListCards';
import AdminOptions from './AdminOptions';

class AdminPage extends Component {
	render() {
		return (
			<div className="container">
				<ListCard
					heading="AdminPage"
					content={
						<AdminOptions />
					}
				/>
			</div>
		);
	}
}

// const mapStateToProps = state => ({
// 	authUser: state.sessionState.authUser,
// 	ChatID: state.Message.ChatID,
// 	UserMessages: state.Message.UserMessages,
// });
// const mapDispatchToProps = ({
// 	GetUserMessages,
// });

export default AdminPage;
