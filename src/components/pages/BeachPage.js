import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import BeachContent from '../shared/content/BeachContent';
import * as routes from '../../constants/routes';

class BeachPage extends Component {
	render() {
		if (!this.props.authUser) {
			return <Redirect to={routes.LANDINGPAGE} />;
		}
		return (
			<div className="container">
				<div className="row">
					<div className="col-sm-12 ">
						<BeachContent />
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});

export default connect(mapStateToProps)(BeachPage);
