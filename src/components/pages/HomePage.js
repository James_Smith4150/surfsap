import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as routes from '../../constants/routes';

import HomeSideContent from '../shared/content/HomeSideContent';
import HomeMainContent from '../shared/content/HomeMainContent';

class DashboardPage extends Component {
	render() {
		if (!this.props.authUser) {
			return <Redirect to={routes.LANDINGPAGE} />;
		}
		return (
			<div className="container content">
				<div className="row">
					<div className="col-sm-9 ">
						<HomeMainContent />
					</div>
					<div className="col-lg-3 ">
						<HomeSideContent />
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});


export default connect(mapStateToProps)(DashboardPage);
