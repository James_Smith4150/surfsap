import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import SigninForm from '../shared/auth/SigninForm';
import SignupForm from '../shared/auth/SignupForm';
import * as routes from '../../constants/routes';


class LandingPage extends Component {
	render() {
		if (this.props.authUser) {
			return <Redirect to={routes.HOMEPAGE} />;
		}
		return (
			<div className="LandingP col-sm-12" >
				{this.props.ShowSignInInd && <SigninForm />}
				{this.props.ShowSignUpInInd && <SignupForm />}
				<Footer />
			</div>
		);
	}
}

const Footer = () => (
	<div className="pageFooter">
		<p>Powered by: Controlrobots</p>
	</div>);


const mapStateToProps = state => ({
	ShowSignUpInInd: state.Interface.ShowSignUpInInd,
	ShowSignInInd: state.Interface.ShowSignInInd,
	UserAdmin: state.auth.UserAdmin,
	authUser: state.sessionState.authUser,
});

export default connect(mapStateToProps)(LandingPage);
