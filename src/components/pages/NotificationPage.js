import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as routes from '../../constants/routes';
import NotificationContent from '../shared/content/NotificationContent'

class NotificationPage extends Component {
	render() {
		if (!this.props.authUser) {
			return <Redirect to={routes.LANDINGPAGE} />;
		}
		return (
			<div className="container">
				<NotificationContent />
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});


export default connect(mapStateToProps)(NotificationPage);
