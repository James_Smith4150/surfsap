import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as routes from '../../constants/routes';
import ProfileContent from '../shared/content/ProfileContent';

class ProfilePage extends Component {
	render() {
		if (!this.props.authUser) {
			return <Redirect to={routes.LANDINGPAGE} />;
		}
		return (
			<div className="container">
				<ProfileContent />
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});


export default connect(mapStateToProps)(ProfilePage);
