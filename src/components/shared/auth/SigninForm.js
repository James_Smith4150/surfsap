import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { RedirectToSignUp } from '../../../reducers/InterfaceReducer';
import { doUserSignIn } from '../../../reducers/AuthenticationReducer';
import { validateEmail, validateMinLength5, validateRequired } from '../../../utils/validate';
import ElementLabelErrorInput from './elements/ElementLabelErrorInput';
import * as routes from '../../../constants/routes';


class SigninForm extends Component {
	constructor() {
		super();
		this.RedirectToSignUp = this.RedirectToSignUp.bind(this);
	}
	RedirectToSignUp() {
		this.props.RedirectToSignUp();
	}
	submitFormUser(e) {
		this.props.doUserSignIn(e.email, e.password);
	}
	render() {
		if (this.props.UserAdmin) {
			return <Redirect to={routes.HOMEPAGE} />;
		}
		const {
			handleSubmit, pristine, submitting, invalid,
		} = this.props;
		return (
			<div className="container spaceTop">
				<form onSubmit={handleSubmit(this.submitFormUser.bind(this))}>
					<h1>Welcome to Surfsap</h1>
					<div className="FormRegister">
						<div className="form-group">
							<div className="container-fluid">
								<div className="col">
									<div className="row">
										<Field
											label="Email"
											name="email"
											component={ElementLabelErrorInput}
											type="text"
											placeholder=""
											validate={[validateMinLength5, validateRequired, validateEmail]}
										/>
									</div>
									<div className="row pb-3">
										<Field
											label="Password"
											name="password"
											component={ElementLabelErrorInput}
											type="password"
											placeholder=""
											validate={[validateMinLength5, validateRequired]}
										/>
									</div>
									<button
										type="submit"
										className="social-signin"
										disabled={pristine || invalid || submitting}
									>
                    Login
									</button>
									<button
										type="button"
										onClick={this.RedirectToSignUp}
										className="social-signin"
									>
                    Signup
									</button>
									<button type="button" className="social-signin twitter">
                    Log in with twitter
									</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	}
}

SigninForm.propTypes = {
	handleSubmit: PropTypes.func.isRequired,
	pristine: PropTypes.bool.isRequired,
	submitting: PropTypes.bool.isRequired,
	invalid: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
	UserAdmin: state.sessionState.UserAdmin,
});

const mapDispatchToProps = {
	doUserSignIn,
	RedirectToSignUp,
};

const AuthLoginClass = connect(mapStateToProps, mapDispatchToProps)(SigninForm);

export default reduxForm({
	form: 'SigninForm',
})(AuthLoginClass);
