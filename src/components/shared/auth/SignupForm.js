import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { validateEmail, validateMinLength5, validateRequired, validateUserName } from '../../../utils/validate';
import ElementLabelErrorInput from './elements/ElementLabelErrorInput';
import { RedirectToSignIn } from '../../../reducers/InterfaceReducer';
import { doUserSignUp } from '../../../reducers/AuthenticationReducer';
import DropDownSelect from './elements/DropDownSelect';

// const InitialState = {
// 	email: '', password: '', username: '', firstName: '', lastName: '', date: '', city: '', ConfirmPassword: '',
// };

class SignupForm extends Component {
	constructor() {
		super();
		this.RedirectToSignin = this.RedirectToSignin.bind(this);
		// this.state = {
		// 	...InitialState,
		// };
	}
	RedirectToSignin() {
		this.props.RedirectToSignIn();
	}
	submitFormUser(e) {
		this.props.doUserSignUp(
			e.email, e.password, e.username,
			e.firstName, e.lastName, e.date, e.dropDownSelect,
		);
	}
	render() {
		const {
			handleSubmit, pristine, submitting, invalid,
		} = this.props;
		return (
			<div className="container SignupFormpadding">
				<form className="signupform" onSubmit={handleSubmit(this.submitFormUser.bind(this))}>
					<h1>Welcome to Surfsap</h1>
					<div className="divForm col-sm-12">
						<div className="row">
							<div className="col-sm-6">
								<Field
									label="Email"
									name="email"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateMinLength5, validateRequired, validateEmail]}
								/>
							</div>
							<div className="col-sm-6">
								<Field
									label="Username"
									name="username"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateUserName, validateRequired]}
								/>
							</div>
						</div>

						<div className="row">
							<div className="col-sm-6">
								<Field
									label="Password"
									name="password"
									component={ElementLabelErrorInput}
									type="password"
									placeholder=""
									validate={[validateMinLength5, validateRequired]}
								/>
							</div>
							<div className="col-sm-6">
								<Field
									label="Confirm Password"
									name="confirmPassword"
									component={ElementLabelErrorInput}
									type="password"
									placeholder=""
									validate={[validateMinLength5, validateRequired]}
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-6">
								<Field
									label="First Name"
									name="firstName"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateRequired]}
								/>
							</div>
							<div className="col-sm-6">
								<Field
									label="Last Name"
									name="lastName"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateRequired]}
								/>
							</div>
						</div>

						<div className="row">
							<div className="col-sm-6">
								<Field
									label="Date of birth"
									name="date"
									component={ElementLabelErrorInput}
									type="date"
									placeholder=""
									validate={[validateRequired]}
								/>
							</div>
							<div className="col-sm-6">
								<Field
									name="dropDownSelect"
									label="City"
									component={DropDownSelect}
									validate={[validateRequired]}
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-4">
								<button
									type="submit"
									className="social-signin"
									disabled={pristine || invalid || submitting}
								>Signup
								</button>
							</div>
							<div className="col-sm-4">
								<button type="button" className="social-signin twitter">
								Sign up with twitter
								</button>
							</div>
							<div className="col-sm-4">
								<button
									type="button"
									className="social-signin"
									onClick={this.RedirectToSignin}
								>Login
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	}
}

const mapDispatchToProps = {
	RedirectToSignIn, doUserSignUp,
};


const AuthSignUpClass = connect(null, mapDispatchToProps)(SignupForm);

export default reduxForm({
	form: 'SignupForm',
})(AuthSignUpClass);
