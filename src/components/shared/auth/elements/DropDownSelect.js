import React from 'react';
import { faTimes } from '@fortawesome/fontawesome-free-solid';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';


class DropDownSelect extends React.Component {
	render() {
		const { input, label, meta: { touched, error } } = this.props;
		return (
			<div className="ElementLabelErrorInput w-100">
				<div className="top text-left">
					<label className="mb-0 text-nowrap" htmlFor={label}>{label}</label>
				</div>
				<div className="mid pb-1 marginTop-15px">
					<select {...input}>
						<option value="">Choose Country</option>
						<option>South Africa</option>
						<option>USA</option>
                        className={`form-control ${(error && touched) ? ' is-invalid ' : ''}`}
					</select>
					<div className="bottom text-left">
						{touched && error &&
						<span className="errorText text-nowrap">
							<FontAwesomeIcon icon={faTimes} />
							{error}
						</span>}
					</div>
				</div>
			</div>
		);
	}
}

export default DropDownSelect;
