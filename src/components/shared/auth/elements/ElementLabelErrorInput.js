import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { uniqueId } from 'lodash';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/fontawesome-free-solid';

class ElementLabelErrorInput extends Component {
	render() {
		// Get the prop values that are passed in from redux-form
		const {
			input, label, type, placeholder, meta: { touched, error },
		} = this.props;
		// Create a unique ID for the label
		const unique = uniqueId('id');
		console.log(this.props);
		return (
			<div className="ElementLabelErrorInput w-100">
				<div className="top text-left">
					<label className="mb-0 text-nowrap" htmlFor={unique}>{label}</label>
				</div>
				<div className="mid pb-1 marginTop-15px">
					<input
						{...input}
						placeholder={placeholder}
						type={type}
						id={unique}
						// Show the is-invalid class if there is an error and if the input has been used
						className={`form-control ${(error && touched) ? ' is-invalid ' : ''}`}
					/>
				</div>
				<div className="bottom text-left">
					{touched && error &&
					<span className="errorText text-nowrap">
						<FontAwesomeIcon icon={faTimes} />
						 {error}
					</span>}
				</div>
			</div>);
	}
}

// The input is what came through as props
ElementLabelErrorInput.propTypes = {
	input: PropTypes.shape({
		name: PropTypes.string.isRequired,
		value: PropTypes.string.isRequired,
		onBlur: PropTypes.func.isRequired,
		onChange: PropTypes.func.isRequired,
		onDragStart: PropTypes.func.isRequired,
		onDrop: PropTypes.func.isRequired,
	}).isRequired,
	label: PropTypes.string.isRequired,
	type: PropTypes.string.isRequired,
	placeholder: PropTypes.string.isRequired,
	meta: PropTypes.shape({
		touched: PropTypes.bool.isRequired,
		error: PropTypes.string,
	}).isRequired,
};

export default ElementLabelErrorInput;
