import React, { Component } from 'react';
// import { connect } from 'react-redux';
import placeholder from '../../../images/placeholder.jpg';

class FileUploader extends Component {
	constructor() {
		super();
		this.state = {
			imageUrl: `url(${placeholder})`,
		};
		this.HandleImageChange = this.HandleImageChange.bind(this);
	}
	HandleImageChange(image) {
		const reader = new FileReader();
		const file = image.files[0];
		reader.onloadend = () => {
			this.setState({ imageUrl: `url(${reader.result})` });
		};
		reader.readAsDataURL(file);
	}
	render() {
		const background = {
			backgroundImage: this.state.imageUrl,
			backgroundRepeat: 'no-repeat',
			backgroundSize: '100% 100%',
		};

		//	console.log(this.state.imageUrl);

		return (
			<div className="fileUpload btn btn-primary" style={background}>
				<span>Upload</span>
				<input
					type="file"
					accept=".jpg, .jpeg, .png"
					className="upload"
					disabled={!this.props.Disable}
					onChange={e => this.HandleImageChange(e.target)}
				/>
			</div>
		);
	}
}


export default FileUploader;
