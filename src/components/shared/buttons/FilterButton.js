import React, { Component } from 'react';
import { connect } from 'react-redux';
import Dropdown from '../dropdowns/Dropdown';
import { setBeachFilter } from '../../../reducers/BeachReducer';

class FilterButtons extends Component {
	constructor() {
		super();
		this.state = {
			beachKey: null,
			beachValue: null,
		};
		this.setbeachValue = this.setbeachValue.bind(this);
		this.setbeachKey = this.setbeachKey.bind(this);
		this.applyFilter = this.applyFilter.bind(this);
	}
	setbeachValue(value) {
		this.state.beachValue = value;
	}
	setbeachKey(value) {
		this.state.beachKey = value;
	}
	applyFilter() {
		if (this.state.beachKey && this.state.beachValue) {
			this.props.setFilterCriteria(true, this.state.beachKey, this.state.beachValue);
		}
	}
	render() {
		const Cities = { 1: 'CapeTown', 2: 'Port Elizerbeth', 3: 'Durban' };
		const Country = { 1: 'Country', 2: 'City' };
		return (
			<div className="row contentSearch">
				<div className="col-sm-3 ">
					<Dropdown
						DropdownPlaceholder="Filter by"
						DropdownList={Country}
						ChangeHandler={this.setbeachKey}
					/>
				</div>
				<div className="col-sm-3">
					<Dropdown
						DropdownPlaceholder="Choose/Country"
						DropdownList={Cities}
						ChangeHandler={this.setbeachValue}
					/>
				</div>
				<div className="col-sm-3 justify-content-center contentSearch">
					<button onClick={this.applyFilter} >Filter</button>
				</div>
				<div className="col-sm-3 contentSearch">
					<input type="text" placeholder="search..." />
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	beachFilter: state.BeachList.beachFilter,
	beachKey: state.BeachList.beachKey,
	beachValue: state.BeachList.beachValue,
});

const mapDispatchToProps = dispatch => ({
	setFilterCriteria: (beachFilter, beachKey, beachValue) =>
		dispatch(setBeachFilter(beachFilter, beachKey, beachValue)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FilterButtons);
