import React, { Component } from 'react';

class GridButtons extends Component {
	render() {
		let pDisabled = 'page-item';
		let nDisabled = 'page-item';

		if (this.props.PageNumber <= 1) pDisabled = 'page-item disabled';
		else if (this.props.PageNumber >= this.props.PageTotal) nDisabled = 'page-item disabled';

		return (
			<nav aria-label="Page navigation example">
				<ul className="pagination justify-content-center">
					<li className={pDisabled}>
						<button
							className="page-link"
							onClick={() => { this.props.NextFunction(false); }}
						>
							Previous
						</button>
					</li>
					<li className="page-item disabled">
						<button className="page-link" href="#">{this.props.PageNumber}</button>
					</li>
					<li className="page-item disabled">
						<button className="page-link" href="#">of</button>
					</li>
					<li className="page-item disabled">
						<button className="page-link" href="#">{this.props.PageTotal}</button>
					</li>
					<li className={nDisabled}>
						<button
							className="page-link"
							onClick={() => { this.props.NextFunction(true); }}
						>
							Next
						</button>
					</li>
				</ul>
			</nav>
		);
	}
}

// <li className="page-item disabled"><button className="page-link" href="#">of</button></li>
// <li className="page-item disabled"><button className="page-link" href="#">3</button></li>


export default GridButtons;
