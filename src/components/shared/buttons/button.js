import React, { Component } from 'react';
// import { connect } from 'react-redux';

class CustomButton extends Component {
	render() {
		return (
			<button className={this.props.buttonClass}>
				<span className={this.props.Icon} />
			</button>
		);
	}
}


export default CustomButton;
