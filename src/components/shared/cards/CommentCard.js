import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ConvertDate } from '../../../utils/usefulFunctions';
import header from '../../../images/header.jpg';
import SubmitPost from '../inputs/SubmitPost';
import { commentPost } from '../../../reducers/PostReducer';

class CommentCard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			comments: this.props.Comments || {},
		};
		this.SubmitComment = this.SubmitComment.bind(this);
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.Comments) {
			this.setState({ comments: nextProps.Comments });
		}
	}
	SubmitComment(commentText) {
		this.props.commentPost(
			this.props.PostId,
			commentText,
			this.props.UserAdmin.uid,
			this.props.UserAdmin.displayName,
		);
	}
	render() {
		const $Comments = (
			Object.keys(this.state.comments).reverse().map(key => (
				<div key={key} className="post">
					<div className="wrap-ut pull-left">
						<div className="posttext col-sm-12">
							<div className="row">
								<img src={header} alt="" />
								<h2>
									<button className="PostButton">{this.state.comments[key].displayName}</button>
								</h2>
							</div>
							<p>
								{this.state.comments[key].commentText}
							</p>
						</div>
						<div className="clearfix" />
					</div>
					<div className="postinfo pull-left">
						<div className="time"><i className="fa fa-clock-o" />
							{ConvertDate(this.state.comments[key].date)}
						</div>
					</div>
					<div className="clearfix" />
				</div>)));
		return (
			<div className="commentCard">
				<SubmitPost
					placeholder="Type Comment"
					submitPostFunction={this.SubmitComment}
				/>
				<div className="comments">
					{$Comments}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	UserAdmin: state.sessionState.authUser,
});

const mapDispatchToProps = {
	commentPost,
};


export default connect(mapStateToProps, mapDispatchToProps)(CommentCard);
