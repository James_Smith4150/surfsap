import React, { Component } from 'react';
// import { connect } from 'react-redux';
// import PinnedBeach from './elements/PinnedBeach';

class ListCard extends Component {
	render() {
		return (
			<div className="sidebarblock">
				<h3>{this.props.heading}</h3>
				<div className="divline" />
				<div className="blocktxt">
					{this.props.content }
				</div>
			</div>
		);
	}
}

// const mapStateToProps = state => ({
//
// });			<div classNameName="pinnedBeach">
// const mapDispatchToProps = {
// connect(mapStateToProps, mapDispatchToProps)
// };
//




export default ListCard;
