import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

// import PinnedBeach from './elements/PinnedBeach';
import { ConvertDate, checkMessages } from '../../../utils/usefulFunctions';
import header from '../../../images/header.jpg';
import Modal from '../modals/Modal';
import CommentCard from './CommentCard';
import * as routes from '../../../constants/routes';


class PostCard extends Component {
	constructor(props) {
		super(props);

		this.ViewUserProfile = this.ViewUserProfile.bind(this);
	}
	ViewUserProfile(userID) {
		if (!checkMessages(userID, this.props.authUser.uid))	{
			this.props.history.push(`${routes.PROFILEPAGE}?param=${userID}`);
		}
	}
	render() {
		let commentsnumber = 0;
		if (this.props.Comments) {
			commentsnumber = Object.keys(this.props.Comments).length;
		}
		const date = ConvertDate(this.props.Date);
		return (
			<div className="post">
				<div className="wrap-ut pull-left">
					<div className="posttext col-sm-12">
						<div className="row">
							<img src={header} alt="" />
							<h2>
								<button
									onClick={() => this.ViewUserProfile(this.props.userid)}
									className="PostButton"
								>
									{this.props.Name}
								</button>
							</h2>
						</div>
						<hr />
						<p>
							{this.props.posttext}
						</p>
					</div>
					<div className="clearfix" />
				</div>
				<div className="postinfo pull-left">
					<div className="comments">
						<Modal
							ModalHeading="comments"
							DisplayComponent={
								<CommentCard PostId={this.props.PostID} Comments={this.props.Comments} />}
							ModalButton={commentsnumber}
							className="commentbg"
						/>
					</div>
					<div className="views">
						<button
							onClick={() => { this.props.Likebutton(this.props.PostID, this.props.Likes); }}
							className="PostButton fa fa-angellist"
						/>
						{this.props.Likes}
					</div>
					<div className="time"><i className="fa fa-clock-o" />{date}</div>
				</div>
				<div className="clearfix" />
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});



export default connect(mapStateToProps, null)(withRouter(PostCard));
