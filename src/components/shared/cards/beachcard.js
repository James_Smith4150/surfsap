import React, { Component } from 'react';

class BeachCard extends Component {
	render() {
		return (
			<div className="beach-card">
				<div className="card-info">
					<div className="row">
						{this.props.description}
					</div>
					<div className="row">
						<button>View Map</button>
						<button>Add To Favourites</button>
						<button>Like</button>
						<button>Check In</button>
					</div>
				</div>

				<div className="utility-info">
					<ul className="utility-list">
						<li><span className="fa fa-star">{this.props.ratings}</span> Likes</li>
						<li><span className="fa fa-plane">{this.props.visits}</span> Visits</li>
					</ul>
				</div>

				<div className="gradient-overlay"></div>
				<div className="color-overlay"></div>
			</div>
		)
	}
}

export default BeachCard
