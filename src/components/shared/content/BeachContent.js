import React, { Component } from 'react';
import { connect } from 'react-redux';
import BeachCard from '../cards/beachcard';
import GridButtons from '../buttons/GridButton';
import FilterButtons from '../buttons/FilterButton';
import { getBeachList } from '../../../reducers/BeachReducer';

class BeachContent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			beaches: {},
		};
		this.props.getBeachList(1, 1);
		this.togglePageGrid = this.togglePageGrid.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.beaches) {
			this.setState({ beaches: nextProps.beaches });
		}
	}

	togglePageGrid(PageToLoad) {
		const filterInd = this.props.beachFilter;
		const filterKey =	this.props.beachKey;
		const filterValue = this.props.beachValue;
		if (PageToLoad) {
			const PageNumber = this.props.beachPageNumber + 1;
			const GetNext = this.props.beachNextItems + 9;
			this.props.getBeachList(GetNext, PageNumber, filterInd, filterKey, filterValue);
		} else {
			const PageNumber = this.props.beachPageNumber - 1;
			if (PageNumber <= 1) {
				this.props.getBeachList(1, PageNumber, filterInd, filterKey, filterValue);
			}	else {
				const GetNext = this.props.beachNextItems - 9;
				this.props.getBeachList(GetNext, PageNumber, filterInd, filterKey, filterValue);
			}
		}
	}

	render() {
		const $BeachCards = (
			Object.keys(this.state.beaches).map(key => (
				<div key={key} className="col-sm-4 beachCardSpacing">
					<p>{key}</p>
					<BeachCard
						description={this.state.beaches[key].description}
						visits={this.state.beaches[key].visits}
						ratings={this.state.beaches[key].visits}
					/>
				</div>)));

		return (
			<div className="container-fluid" >
				<FilterButtons />
				<div className="row" >
					{ $BeachCards }
				</div>
				<GridButtons
					NextFunction={this.togglePageGrid}
					PreviousFunction={this.togglePageGrid}
					PageNumber={this.props.beachPageNumber}
					PageTotal={this.props.beachPageTotal}
				/>
			</div>
		);
	}
}


const mapStateToProps = state => ({
	beaches: state.BeachList.beaches,
	beachPageNumber: state.BeachList.beachPageNumber,
	beachPageTotal: state.BeachList.beachPageTotal,
	beachNextItems: state.BeachList.beachNextItems,
	beachFilter: state.BeachList.beachFilter,
	beachKey: state.BeachList.beachKey,
	beachValue: state.BeachList.beachValue,
});

const mapDispatchToProps = {
	getBeachList,
};

export default connect(mapStateToProps, mapDispatchToProps)(BeachContent);
