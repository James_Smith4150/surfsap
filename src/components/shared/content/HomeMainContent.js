import React, { Component } from 'react';
import { connect } from 'react-redux';
import PostCard from '../cards/PostCard';
import SubmitPost from '../inputs/SubmitPost';
import { GetSubmissionsPost, likePost, submitPost } from '../../../reducers/PostReducer';

class HomeMainContent extends Component {
	constructor(props) {
		super(props);
		this.props.GetSubmissionsPost();
		this.state = {
			Userpost: this.props.Userpost,
		}
		this.handleLikes = this.handleLikes.bind(this)
		this.SubmitPost = this.SubmitPost.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.Userpost !== {}) {
			this.setState({ Userpost: nextProps.Userpost })
		}
	}

	handleLikes(id, likes) {
		this.props.likePost(id, likes)
	}
	SubmitPost(posttext) {
		this.props.submitPost(posttext, this.props.UserAdmin.uid, this.props.UserAdmin.displayName);
	}
	render() {
		const $PostCards = (
			Object.keys(this.state.Userpost).reverse().map(key => (
				<div key={key} >
					<PostCard
						Name={this.props.Userpost[key].displayName}
						Date={this.props.Userpost[key].date}
						Likes={this.props.Userpost[key].likes}
						posttext={this.props.Userpost[key].posttext}
						userid={this.props.Userpost[key].userID}
						Likebutton={this.handleLikes}
						PostID={key}
						Comments={this.props.Userpost[key].comments}
					/>
				</div>))
		);
		return (
			<div>
				<SubmitPost
					placeholder="Share your waves"
					submitPostFunction={this.SubmitPost}
				/>
				{$PostCards}
			</div>
		);
	}
}
const mapStateToProps = state => ({
	Userpost: state.SubmitPost.Userpost,
	UserAdmin: state.sessionState.authUser,
});
const mapDispatchToProps = {
	GetSubmissionsPost,
	likePost,
	submitPost
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeMainContent);
