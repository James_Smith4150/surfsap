import React, { Component } from 'react';
import { connect } from 'react-redux';
import ListCard from '../cards/ListCards';
import SurfingPoll from '../lists/SurfingPoll'
import TopBeaches from '../lists/TopBeaches'

import { getBeachPollList, getTopRatedBeachList, VisitBeachL, RateBeachL } from '../../../reducers/BeachReducer'


class HomeSideContent extends Component {
	constructor(props) {
		super(props);
		this.SurfingFunction = this.SurfingFunction.bind(this)
		this.props.getBeachPollList(7)
		this.props.getTopRatedBeachList(7)
		this.state = {
			beachpoll: {},
			topRated: {},
		};
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.beachPoll) {
			this.setState({ beachpoll: nextProps.beachPoll })
			console.log(nextProps.beachPoll);
		}
		if (nextProps.topRated) {
			this.setState({ topRated: nextProps.topRated })
		}
	}
	SurfingFunction(id, likeRating, isRating) {
		if (isRating) {
			this.props.RateBeachL(id, likeRating);
		} else {
			this.props.VisitBeachL(id, likeRating);
		}
	}
	render() {
		return (
			<div>
				<ListCard
					content={
						<SurfingPoll
							content={this.state.beachpoll}
							surfingFunction={this.SurfingFunction}
						/>
					}
					heading="Top Visited Beaches"
				/>

				<ListCard
					content={
						<TopBeaches
							content={this.state.topRated}
							surfingFunction={this.SurfingFunction}
						/>}
					heading="Top Rated Beaches"
				/>
			</div>
		);
	}
}


const mapStateToProps = state => ({
	beachPoll: state.BeachList.beachPoll,
	topRated: state.BeachList.topRated,
});

// <div className="pinnedBeach">

const mapDispatchToProps = ({
	getBeachPollList,
	getTopRatedBeachList,
	VisitBeachL,
	RateBeachL,
});


export default connect(mapStateToProps, mapDispatchToProps)(HomeSideContent);
