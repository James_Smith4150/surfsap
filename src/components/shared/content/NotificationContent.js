import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import MessageList from './elements/MessageList';
import MessageScreen from './elements/MessageScreen';
import ListCard from '../cards/ListCards';
import { GetUserMessages } from '../../../reducers/MessageReducer';

const queryString = require('query-string');

class NotificationContent extends Component {
	constructor(props) {
		super(props);
		const parsed = queryString.parse(this.props.location.search).param;
		const parsedUID = queryString.parse(this.props.location.search).param2;
		this.state = {
			ShowMessage: parsed || false,
			ChatID: '',
			user2: parsedUID,
			UserMessages: {}
		};
		this.ToggleMessgaeScreen = this.ToggleMessgaeScreen.bind(this);
		this.props.GetUserMessages(this.props.authUser.uid);
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.ChatID) this.setState({ ChatID: nextProps.ChatID });
		if (nextProps.UserMessages) this.setState({ UserMessages: nextProps.UserMessages });
	}

	ToggleMessgaeScreen() {
		this.setState({ ShowMessage: !this.state.ShowMessage });
	}
	render() {
		const $Chats = (
			Object.keys(this.state.UserMessages).map(key => (
				<div key={key} className="col-sm-2">
					<MessageList Messagebtnfunction={this.ToggleMessgaeScreen} />
				</div>
			)));
		return (
			<div className="container">
				{!this.state.ShowMessage && <ListCard
					heading="Messages"
					content={
						<div className="row">
							{$Chats}
						</div>
					}
				/>}
				{this.state.ShowMessage && <ListCard
					heading={this.props.authUser.displayName}
					content={
						<div>
							<MessageScreen
								chatID={this.state.ChatID}
								user1={this.props.authUser.uid}
								user2={this.state.user2}
							/>
						</div>}
				/>}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
	ChatID: state.Message.ChatID,
	UserMessages: state.Message.UserMessages,
});
const mapDispatchToProps = ({
	GetUserMessages,
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NotificationContent));
