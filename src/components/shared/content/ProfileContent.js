import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ProfileSideTabs from './elements/ProfileSideTabs';
import ProfileForm from './elements/ProfileForm';
import ViewProfile from './elements/ViewProfile';
import PostCard from '../cards/PostCard';

// Reducer Improts
import { doUserUpdate, getUserProfile, getAuthUserDeatils } from '../../../reducers/ProfileReducer';

const queryString = require('query-string');

class ProfileContent extends Component {
	constructor(props) {
		super(props);
		const parsed = queryString.parse(this.props.location.search).param;
		this.state = {
			MyProfile: true,
			Userid: parsed,
			ActiveTab: 1,
			EditProfile: false,
		};
		if (parsed) {
			this.props.getUserProfile(parsed);
		} else {
			this.props.getUserProfile(this.props.authUser.uid);
		}
		this.TabHandler = this.TabHandler.bind(this);
		this.ToggleEdit = this.ToggleEdit.bind(this);
	}
	componentWillMount() {
		const parsed = queryString.parse(this.props.location.search).param;
		if (parsed) {
			this.setState({ MyProfile: false, Userid: parsed });
		} else {
			this.setState({ MyProfile: true, Userid: this.props.authUser.uid });
		}
	}

	TabHandler(num) {
		this.setState({ ActiveTab: num });
	}
	ToggleEdit(booleanVisible) {
		this.setState({ EditProfile: booleanVisible });
	}
	render() {
		return (
			<div className="ProfileContent">
				<div className="col-sm-12 profileTabs">
					<ProfileSideTabs
						tabFunction={this.TabHandler}
						editFunction={this.ToggleEdit}
						hideButton={this.state.EditProfile}
						UserProfile={this.props.UserProfile}
						ViewingProfile={this.state.MyProfile}
						UserUID={this.state.Userid}
					/>
				</div>
				<div className="row">
					{this.state.ActiveTab === 1 && !this.state.EditProfile &&
						<ViewProfile
							UserProfile={this.props.UserProfile}
							UserAuth={this.props.authUser}
						/> }
					{this.state.ActiveTab === 1 && this.state.EditProfile &&
					<ProfileForm
						UserProfile={this.props.UserProfile}
						UserAuth={this.props.authUser}
					/> }
					{this.state.ActiveTab === 2 && <PostCard /> }
					{this.state.ActiveTab === 3 && <PostCard /> }
				</div>

			</div>
		);
	}
}

const mapStateToProps = state => ({
	UserProfile: state.profile.UserProfile,
	UserAuth: state.profile.UserAuth,
	authUser: state.sessionState.authUser,
});
const mapDispatchToProps = {
	doUserUpdate,
	getUserProfile,
	getAuthUserDeatils,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProfileContent));
