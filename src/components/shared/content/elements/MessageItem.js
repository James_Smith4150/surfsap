import React, { Component } from 'react';

class MessageItem extends Component {
	render() {
		let status = 'row';
		if (this.props.status) {
			status = 'row sent'
		} else {
			status = 'row received'
		}
		return (
			<div className={status}>
				<div className="col-sm-7 messText">
					<p>{this.props.messageText}</p>
				</div>
			</div>
		);
	}
}


export default MessageItem;
