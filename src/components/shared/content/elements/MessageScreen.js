import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessageItem from './MessageItem';
import placeholder from '../../../../images/placeholder.jpg';
import { SubmitMessage, GetChatMessages } from '../../../../reducers/MessageReducer';
import { checkMessages } from '../../../../utils/usefulFunctions';

class MessageScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			messageText: '',
			messagesList: {},
		};
		this.SendMessage = this.SendMessage.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.chatID) this.props.GetChatMessages(nextProps.chatID);
		if (nextProps.ChatMessage) {
			this.setState({ messagesList: nextProps.ChatMessage });
		}
	}

	SendMessage(messageText, e) {
		e.preventDefault();
		this.props.SubmitMessage(this.props.chatID, messageText, this.props.user1, this.props.user2);
	}
	render() {
		const $messagesList = (
			Object.keys(this.state.messagesList).map(key => (
				<div key={key} >
					<MessageItem
						status={checkMessages(this.state.messagesList[key].to, this.props.authUser.uid)}
						ownerImage={placeholder}
						messageText={this.state.messagesList[key].messageText}
					/>
				</div>)));
		const { messageText } = this.state;
		return (
			<div className="container messageScreen">
				<div className="messageDisplay row justify-content-center">
					<div className="col-sm-12">
						{$messagesList}
					</div>
				</div>
				<hr />
				<div className="messageB row justify-content-center">
					<div className="col-sm-10">
						<form>
							<textarea
								placeholder="type message"
								className="pull-left"
								value={messageText}
								onChange={e => this.setState({ messageText: e.target.value })}
							/>
							<button
								onClick={e => this.SendMessage(messageText, e)}
								className="pull-left"
							>
							Send
							</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
	ChatMessage: state.Message.ChatMessage,
});
const mapDispatchToProps = ({
	SubmitMessage,
	GetChatMessages,

});

export default connect(mapStateToProps, mapDispatchToProps)(MessageScreen);
