import React, { Component } from 'react';
// import { connect } from 'react-redux';
import placeHolder from '../../../../images/placeholder.jpg';

class PinnedBeach extends Component {
	render() {
		return (
			<div className="container-fluid margPadZero">
				<div className="beachImageContainer">
					<img src={placeHolder} alt="beach" />
				</div>
			</div>
		);
	}
}

// const mapStateToProps = state => ({
//
// });
// const mapDispatchToProps = {
// connect(mapStateToProps, mapDispatchToProps)
// };

export default PinnedBeach;
