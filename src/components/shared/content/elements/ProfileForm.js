// React Imports
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { doUserUpdate, getUserProfile } from '../../../../reducers/ProfileReducer'
// Utils Imports
import { validateEmail, validateMinLength5, validateRequired, validateUserName } from '../../../../utils/validate';
import ElementLabelErrorInput from '../../../shared/auth/elements/ElementLabelErrorInput';
import DropDownSelect from '../../../shared/auth/elements/DropDownSelect';
// import LinearIndeterminate from '../shared/auth/elements/LoadingBar';
// import Spinner from '../../../shared/spinners/Spinner';

// const queryString = require('query-string')


class Profile extends Component {
	componentWillReceiveProps(nextProps) {
		if (nextProps.UserProfile) {
			this.props.initialize({
				email: this.props.authUser.email,
				username: this.props.authUser.displayName,
				firstname: nextProps.UserProfile.firstName,
				lastname: nextProps.UserProfile.lastName,
				Dateofbirth: nextProps.UserProfile.age,
				dropDownSelect: nextProps.UserProfile.country,
			});
		}
	}

	saveUser(e) {
		this.props.doUserUpdate(
			this.props.UserAdmin.uid, e.firstname, e.lastname,
			e.Dateofbirth, e.dropDownSelect, e.username, e.email,
		);
	}
	render() {
		const {
			handleSubmit, pristine, submitting, invalid,
		} = this.props;
		return (
			<div className="container ">
				<form onSubmit={handleSubmit(this.saveUser.bind(this))}>
					<div className="col-sm-12">
						<div className="row">
							<div className="col-sm-6">
								<Field
									label="Email"
									name="email"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateMinLength5, validateRequired, validateEmail]}
								/>
							</div>
							<div className="col-sm-6">
								<Field
									label="Username"
									name="username"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateUserName, validateRequired]}
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-6">
								<Field
									label="First Name"
									name="firstname"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateRequired]}
								/>
							</div>
							<div className="col-sm-6">
								<Field
									label="Last Name"
									name="lastname"
									component={ElementLabelErrorInput}
									type="text"
									placeholder=""
									validate={[validateRequired]}
								/>
							</div>
						</div>

						<div className="row">
							<div className="col-sm-6">
								<Field
									label="Date of birth"
									name="Dateofbirth"
									component={ElementLabelErrorInput}
									type="date"
									placeholder=""
									validate={[validateRequired]}
								/>
							</div>
							<div className="col-sm-6">
								<Field
									name="dropDownSelect"
									label="Country"
									component={DropDownSelect}
									validate={[validateRequired]}
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-sm-4">
								<button
									type="submit"
									className="social-signin"
									disabled={pristine || invalid || submitting}
								>Save
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	UserProfile: state.profile.UserProfile,
	authUser: state.sessionState.authUser,
});

const mapDispatchToProps = {
	doUserUpdate,
	getUserProfile,
};

const ProfileForm = connect(mapStateToProps, mapDispatchToProps)(Profile);

export default reduxForm({
	form: 'Profile',
})(ProfileForm);
