import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import FileUploader from '../../buttons/FileUploader';
import { CreateChat } from '../../../../reducers/MessageReducer';
import * as routes from '../../../../constants/routes';

class ProfileSideTabs extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstName: '',
			lastName: '',
		};
		this.handleSendMessage = this.handleSendMessage.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.UserProfile) {
			this.setState({ firstName: nextProps.UserProfile.firstName });
			this.setState({ lastName: nextProps.UserProfile.lastName });
		}
	}

	handleSendMessage() {
		if (this.props.UserUID.localeCompare(this.props.UserAuth.uid === -1)) {
			this.props.CreateChat(this.props.UserUID, this.props.UserAuth.uid, false);
			this.props.history.push(`${routes.NOTIFICATIONPAGE}?param=true&param2=${this.props.UserUID}`);
		} else {
			this.props.CreateChat(this.props.UserAuth.uid, this.props.UserUID, true);
			this.props.history.push(`${routes.NOTIFICATIONPAGE}?param=true&param2=${this.props.UserUID}`);
		}
	}
	render() {
		return (
			<div className="">
				<div className="row UserInfo">
					<div className="col-sm-2 pictureShrink">
						<FileUploader Disable={this.props.ViewingProfile} />
					</div>
					<div className="col-sm-8">
						<p className="userProfileName">{this.state.firstName} {this.state.lastName}</p>
						<p className="">Enjoy surfing, being under water has always been a passion to me</p>
					</div>
					<div className="col-sm-2">
						{this.props.ViewingProfile && !this.props.hideButton &&
							<button
								type="button"
								className="userProfileButton"
								onClick={() => this.props.editFunction(true)}
							>
							Edit Profile
							</button>}

						{this.props.ViewingProfile && this.props.hideButton &&
							<button
								type="button"
								className="userProfileButton"
								onClick={() => this.props.editFunction(false)}
							>
							Save Profile
							</button>}

						{!this.props.ViewingProfile &&
							<button type="button" className="userProfileButton" onClick={this.handleSendMessage}>
								Send Message
							</button>}
					</div>
				</div>
				<hr />
				<div className="row  justify-content-center">
					<div className="naveItmes">
						<button onClick={() => this.props.tabFunction(1)}>Profile</button>
					</div>
					<div className="naveItmes">
						<button onClick={() => this.props.tabFunction(2)}>Post</button>
					</div>

					{this.props.ViewingProfile &&
						<div className="naveItmes">
							<button onClick={() => this.props.tabFunction(3)}>Message</button>
						</div>}
				</div>
			</div>
		);
	}
}
const mapStateToProps = state => ({
	UserAuth: state.sessionState.authUser,
});

const mapDispatchToProps = {
	CreateChat,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProfileSideTabs));
