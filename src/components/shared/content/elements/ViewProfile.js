// React Imports
import React, { Component } from 'react';


class ViewProfile extends Component {
	constructor() {
		super();
		this.state = {
			Userprofile: { },
		//	userAuth: {},
		};
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.UserProfile) {
			this.setState({ Userprofile: nextProps.UserProfile });
		}
		if (nextProps.UserAuth) {
			// this.setState({ userAuth: nextProps.UserAuth });
		}
	}

	render() {
		return (
			<div className="ViewProfile container">
				<div className="row">
					{this.props.UserAuth.displayName}
				</div>
				<div className="row">
					{this.state.Userprofile.firstName} {this.state.Userprofile.lastName}
				</div>
				<div className="row">
					{this.state.Userprofile.age}
				</div>
				<div className="row">
					{this.state.Userprofile.country}
				</div>
			</div>
		);
	}
}

export default ViewProfile;
