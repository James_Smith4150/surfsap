import React from 'react';

class Dropdown extends React.Component {
	render() {
		const $options = (
			Object.keys(this.props.DropdownList).map(key =>
				<option key={key}>{this.props.DropdownList[key]}</option>));
		return (
			<div className="SelectMinWithd">
				<select onChange={(event) => { this.props.ChangeHandler(event.target.value); }}>
					<option>
						{this.props.DropdownPlaceholder}
					</option>
					{$options}
				</select>
			</div>
		);
	}
}

export default Dropdown;
