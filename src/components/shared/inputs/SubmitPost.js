import React, { Component } from 'react';

class SubmitPost extends Component {
	constructor(props) {
		super(props);
		this.state = {
			PostText: '',
		};
		this.SubmitPost = this.SubmitPost.bind(this);
	}
	SubmitPost() {
		this.props.submitPostFunction(this.state.PostText);
		this.setState({ PostText: '' });
	}
	render() {
		return (
			<div className="col-sm-12 postForm">
				<div className="row">
					<div className="col-sm-12 submittextarea">
						<textarea
							value={this.state.PostText}
							onChange={e => this.setState({ PostText: e.target.value })}
							rows="3"
							maxLength="200"
							placeholder={this.props.placeholder}
						/>
						<button type="" onClick={this.SubmitPost} className="">
							<i className="fa fa-pencil-square-o" />Post
						</button>
					</div>
				</div>
				<hr />
			</div>
		);
	}
}

export default SubmitPost;
