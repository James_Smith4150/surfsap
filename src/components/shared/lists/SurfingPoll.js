import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import * as routes from '../../../constants/routes';

class SurfingPoll extends Component {
	render() {
		const $BeachCards = (
			Object.keys(this.props.content).map(key => (
				<li key={key}>
					{key}
					<button
						onClick={() => this.props.surfingFunction(key, this.props.content[key].visits, false)}
						className="badge pull-right buttonTheme cursorPointer"
					>
						{this.props.content[key].visits} Surfers
					</button>
				</li>
			))
		);
		return (
			<ul className="cats">
				{$BeachCards}
				<li>
					<NavLink to={routes.BEACHPAGE}className="badge buttonTheme cursorPointer">
					view all
					</NavLink>
				</li>
			</ul>
		);
	}
}

export default SurfingPoll;
