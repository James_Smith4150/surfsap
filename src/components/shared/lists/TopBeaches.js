import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import * as routes from '../../../constants/routes';

class TopBeaches extends Component {
	render() {
		const $BeachCards = (
			Object.keys(this.props.content).map(key => (
				<li key={key}>
					{key}
					<button
						onClick={() => this.props.surfingFunction(key, this.props.content[key].rating, true)}
						className="badge pull-right themeColourGrey noRadius buttonTheme cursorPointer"
					>
						{this.props.content[key].rating} Surfers
					</button>
				</li>
			))
		);
		return (
			<ul className="cats">
				{$BeachCards}
				<li>
					<NavLink to={routes.BEACHPAGE}className="badge themeColourGrey cursorPointer">
					view all
					</NavLink>
				</li>
			</ul>
		);
	}
}


export default TopBeaches;
