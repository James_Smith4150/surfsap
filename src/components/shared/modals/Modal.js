import React, { Component } from 'react';

class Modal extends Component {
	constructor() {
		super();
		this.state = {
			showModal: false,
		};
		this.IsActiveModal = this.IsActiveModal.bind(this);
	}
	IsActiveModal() {
		this.setState({ showModal: !this.state.showModal });
	}
	render() {
		const btnClass = `${this.props.className} modalbutton`;
		const $ModalContent = (
			<div className="CustomModal">
				<div className="mHeader">
					<h2 className="mTitle">{this.props.ModalHeading}</h2>
					<button onClick={this.IsActiveModal} className="btnClose">X</button>
				</div>
				<div className="modalContent">
					{this.props.DisplayComponent }
				</div>
			</div>
		);
		const $Overly = (
			<button type="button" onClick={this.IsActiveModal} className="colourOverlay">
				<span className="">X</span>
			</button>
		);
		return (
			<div className="container-fluid">
				<button type="button" onClick={this.IsActiveModal} className={btnClass}>
					{this.props.ModalButton}
				</button>
				{this.state.showModal && $ModalContent}
				{this.state.showModal && $Overly}
			</div>
		);
	}
}


export default Modal;
