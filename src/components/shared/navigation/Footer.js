import React, { Component } from 'react';
import { connect } from 'react-redux';
import NavigationItem from './elements/NavigationItem';


class Footer extends Component {
	render() {
		return (
			<nav className="NavigationSecondary footer navbar " >
				<div className="container justify-content-left">
					<NavigationItem
						PageLink="/"
						NavIcon=""
						PageName="Copyright Controlrobots 2017"
					/>
				</div>
				<div className="justify-content-right">
					<NavigationItem
						PageLink="/about"
						NavIcon=""
						PageName="Twitter"
					/>
					<NavigationItem
						PageLink="/about"
						NavIcon=""
						PageName="Facebook"
					/>
				</div>
				<div className="justify-content-right">
					<NavigationItem
						PageLink="/about"
						NavIcon=""
						PageName="About"
					/>
					<NavigationItem
						PageLink="/about"
						NavIcon=""
						PageName="Terms and Conditions"
					/>
				</div>
			</nav>
		);
	}
}

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});

export default connect(mapStateToProps)(Footer);
