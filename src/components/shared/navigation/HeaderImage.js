import React, { Component } from 'react';
import header from '../../../images/header.jpg'

class HeaderImage extends Component {
	render() {
		return (
			<div>
				<div className="container-fluid row header" >
					<div className="headeroverlay"><p>SurfAp</p></div>
					<img src={header} alt="img" />
				</div>
			</div>
		);
	}
}

export default HeaderImage;
