import React, { Component } from 'react';
import { connect } from 'react-redux';
import NavigationItem from './elements/NavigationItem';
import NavigationDropDown from './elements/NavigationDropDown';


class NavigationAppBar extends Component {
	render() {
		return (
			<div className="headernav">
				<div className="container">
					<div className="row">
						<div className="col-sm-8">
							<div className="row navLinksTop">
								<NavigationItem
									PageLink="/home"
									NavIcon="fa fa-home"
									PageName=" HOME"
								/>
								<NavigationItem
									PageLink="/beach"
									NavIcon="fa fa-globe"
									PageName=" BEACHES"
								/>
								<NavigationItem
									PageLink="/about"
									NavIcon="fa fa-info-circle"
									PageName=" ABOUT"
								/>
							</div>
						</div>
						<div className="wrap">
							<div className="row navLinksTop">
								<NavigationItem
									PageLink="/notifications"
									NavIcon="fa fa-envelope"
									PageName=" Notifications"
								/>
								<NavigationDropDown
									NavIcon="fa fa-user"
									ProfileLink="/profile"
									Name={this.props.UserAdmin.displayName}
								/>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	UserAdmin: state.sessionState.authUser,
});

export default connect(mapStateToProps)(NavigationAppBar);
