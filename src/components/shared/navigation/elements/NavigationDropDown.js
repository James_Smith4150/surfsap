import React, { Component } from 'react';
// import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { doSignOut } from '../../../../API/authenthication';
import { getUserProfile } from '../../../../reducers/ProfileReducer';
import * as routes from '../../../../constants/routes';

class NavigationDropDown extends Component {
	constructor() {
		super();
		this.state = {
			show: false,
		}
		this.logoutFunction = this.logoutFunction.bind(this);
		this.toggleDropdown = this.toggleDropdown.bind(this);
		this.NavigateToProfile = this.NavigateToProfile.bind(this);
	}

	logoutFunction() {
		doSignOut();
	}
	toggleDropdown() {
		this.setState({ show: !this.state.show });
	}
	NavigateToProfile() {
		this.setState({ show: !this.state.show });
		this.props.history.push(`${routes.PROFILEPAGE}`);
	}

	render() {
		const style = {
			display: 'block',
		};
		const $DropdownItems = (
			<ul style={style} className="dropdown-menu" role="menu">
				<button onClick={this.NavigateToProfile}> Profile </button>
				<hr />
				<button onClick={this.logoutFunction}> Log out </button>
			</ul>
		);
		return (
			<div className="dropdown">
				<button onClick={this.toggleDropdown} >
					<span className="nav-icon">
						<i className={this.props.NavIcon}>Hello {this.props.Name}</i>
					</span>
				</button>
				{ this.state.show && $DropdownItems }
			</div>
		);
	}
}
// <div className="dropdown">
// 	<span className="nav-icon">
// 		<i className={this.props.NavIcon}>Hello {this.props.Name}</i>
// 	</span>
// 	<div className="dropdown-content">
// 		<ul>
// 			<NavLink to={this.props.ProfileLink}> Profile </NavLink>
// 			<hr />
// 			<NavLink to={this.props.AccountLink}> Account</NavLink>
// 			<hr />
// 			<button onClick={this.logoutFunction}> Log out </button>
// 		</ul>
// 	</div>
// </div>

const mapStateToProps = state => ({
	authUser: state.sessionState.authUser,
});

const mapDispatchToProps = {
	getUserProfile,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NavigationDropDown));
