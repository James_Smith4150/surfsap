import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

class NavigationItem extends Component {
	render() {
		return (
			<NavLink className="nav-link" to={this.props.PageLink} style={this.props.style}>
				<span className="nav-icon">
					<i className={this.props.NavIcon} />
				</span>
				{this.props.PageName}
			</NavLink>
		);
	}
}

export default NavigationItem;
