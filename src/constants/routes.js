export const LANDINGPAGE = '/';
export const HOMEPAGE = '/home';
export const PROFILEPAGE = '/profile';
export const BEACHPAGE = '/beach';
export const NOTIFICATIONPAGE = '/notifications';
export const CHATPAGE = '/chat';
export const ABOUTPAGE = '/about';

export const ADMINPAGE = '/admin';
