import { cloneDeep } from 'lodash';
import { doSignInWithEmailAndPassword, doCreateUserWithEmailAndPassword, doCurrentUserUpdate } from '../API/authenthication';
import { doManageUser } from '../API/database';

export const USER_SIGNIN = 'User_Signin';
export const SIGN_FAIL = 'Sign_Fail';
export const SET_USERPROFILE = 'Set_UserProfile';

// Manage User Details Initial State
const UserDetails = {
	displayName: '',
	email: '',
	photoUrl: '',
	uid: '',
	emailVerified: '',
};

const InitialState = {
	UserAdmin: UserDetails,
	error: '',
};

/* Reducers */
const UserAuthethication = (state = InitialState, action) => {
	switch (action.type) {
		case USER_SIGNIN:
			state = cloneDeep(state);
			state.UserAdmin = action.payload;
			return state;

		case SIGN_FAIL:
			state = cloneDeep(state);
			state.error = action.payload.error;
			return state;

		default:
			return state;
	}
};

/* Actions */
export const SignInUser = User => ({
	type: USER_SIGNIN,
	payload: User,
});

export const SignInFail = error => ({
	type: SIGN_FAIL,
	payload: error,
});

/* Thunk functions which make API Calls and Action Dispatch */
export const doUserSignIn = (email, password) => (dispatch) => {
	doSignInWithEmailAndPassword(email, password)
		.then((() => {
		//	dispatch(SignInUser(authUser));
		}))
		.catch((error) => {
			dispatch(SignInFail(error));
		});
};

export const doUserSignUp = (email, password, username, firstName, lastName, age, country) =>
	(dispatch) => {
		// We create the new user and logging in in the database (NB)
		doCreateUserWithEmailAndPassword(email, password)
			.then(((authUser) => {
				// In this case we are setting that the new user has
				// logged in at the same time as signing up in the Redux Store(NB)
				dispatch(SignInUser(authUser));
				// We are saving the users specific details
				doManageUser(authUser.uid, firstName, lastName, age, country)
					.then(({
					}))
					.catch((error) => {
						dispatch(SignInFail(error));
					});
				doCurrentUserUpdate(username);
			}))
			.catch((error) => {
				dispatch(SignInFail(error));
			});
	};

/* Reducer Exports */
export { UserAuthethication };
