import { cloneDeep } from 'lodash';
import { getBeaches, FilterBeaches, getBeachPoll, getTopRated, VisitBeach, RateBeach } from '../API/database';

export const GET_BEACHLIST = 'Get_Beachlist';
export const GETBEACHLIST_FAIL = 'GetBeachlist_Fail';
export const SETBEACHPAGENUMBER = 'SetBeach_Number';
export const SETBEACHFILTERCRITERIA = 'SetBeach_FilterCriteria';
export const SETBEACHPOLL = 'SetBeachPoll';
export const TOPRATED = 'SetTopRated';



const INITIAL_STATE = {
	beaches: null,
	beachPageNumber: 1,
	beachPageTotal: 1,
	beachNextItems: 1,
	beachFilter: false,
	beachKey: null,
	beachValue: null,
	beachPoll: null,
	topRated: null,
};

function BeachReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case GET_BEACHLIST:
			state = cloneDeep(state);
			state.beaches = action.payload;
			return state;
		case SETBEACHPAGENUMBER:
			state = cloneDeep(state);
			state.beachPageNumber = action.payload.nextNumber;
			state.beachNextItems = action.payload.nextItems;
			return state;
		case SETBEACHFILTERCRITERIA:
			state = cloneDeep(state);
			state.beachFilter = action.payload.bf;
			state.beachKey = action.payload.bk;
			state.beachValue = action.payload.bv;
			return state;
		case SETBEACHPOLL:
			state = cloneDeep(state);
			state.beachPoll = action.payload;
			return state;
		case TOPRATED:
			state = cloneDeep(state);
			state.topRated = action.payload;
			return state;
		case GETBEACHLIST_FAIL:
			state = cloneDeep(state);
			return state;

		default: return state;
	}
}

/* Actions */
export const setBeachList = beachlist => ({
	type: GET_BEACHLIST,
	payload: beachlist,
});

export const setBeachNumber = (NextItems, NextNumber) => ({
	type: SETBEACHPAGENUMBER,
	payload: { nextItems: NextItems, nextNumber: NextNumber },
});

export const setBeachFilter = (beachFilter, beachKey, beachValue) => ({
	type: SETBEACHFILTERCRITERIA,
	payload: {
		bf: beachFilter,
		bk: beachKey,
		bv: beachValue,
	},
});

export const failGetBeachList = beachlist => ({
	type: GETBEACHLIST_FAIL,
	payload: beachlist,
});

export const SetBeachPoll = beachPoll => ({
	type: SETBEACHPOLL,
	payload: beachPoll,
});

export const SettopRated = topRated => ({
	type: TOPRATED,
	payload: topRated,
});



/* Thunk functions which make API Calls and Action Dispatch */
export const getBeachList = (GridItemsNumber, previousNext, filter, value, key) => (dispatch) => {
	if (filter) {
		FilterBeaches(value, key, GridItemsNumber)
			.then(((snapshot) => {
				dispatch(setBeachList(snapshot.val()));
				dispatch(setBeachNumber(GridItemsNumber, previousNext));
			}))
			.catch((error) => {
				dispatch(failGetBeachList(error));
			});
	} else {
		getBeaches(GridItemsNumber)
			.then(((snapshot) => {
				dispatch(setBeachList(snapshot.val()));
				dispatch(setBeachNumber(GridItemsNumber, previousNext));
			}))
			.catch((error) => {
				dispatch(failGetBeachList(error));
			});
	}
};

export const getBeachPollList = limitToLast => (dispatch) => {
	getBeachPoll(limitToLast)
		.then(((snapshot) => {
			dispatch(SetBeachPoll(snapshot.val()));
		}))
		.catch((error) => {
			dispatch(failGetBeachList(error));
		});
};

export const getTopRatedBeachList = limitToLast => (dispatch) => {
	getTopRated(limitToLast)
		.then(((snapshot) => {
			dispatch(SettopRated(snapshot.val()));
		}))
		.catch((error) => {
			dispatch(failGetBeachList(error));
		});
};

export const VisitBeachL = (id, likes) => (dispatch) => {
	VisitBeach(id, likes)
		.then((() => {
			getBeachPoll()
				.then(((snapshot) => {
					dispatch(SetBeachPoll(snapshot.val()));
				}))
				.catch((error) => {
					dispatch(failGetBeachList(error));
				});
		}))
		.catch((error) => {
			dispatch(failGetBeachList(error));
		});
};

export const RateBeachL = (id, likes) => (dispatch) => {
	RateBeach(id, likes)
		.then((() => {
			getTopRated()
				.then(((snapshot) => {
					dispatch(SettopRated(snapshot.val()));
				}))
				.catch((error) => {
					dispatch(failGetBeachList(error));
				});
		}))
		.catch((error) => {
			dispatch(failGetBeachList(error));
		});
};


export default BeachReducer;
