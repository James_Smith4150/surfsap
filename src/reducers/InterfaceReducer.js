import { cloneDeep } from 'lodash';

export const INTERFACE_SIGNUPUSER = 'Interface_SignUpUser';
export const INTERFACE_SIGNINUSER = 'Interface_SignInUser';

const InitialState = {
	ShowSignInInd: true,
	ShowSignUpInInd: false,
};

// takes in state and action
// takes in the state based on the constants
const RegistrationDisplay = (state = InitialState, action) => {
	switch (action.type) {
		case INTERFACE_SIGNUPUSER:
			state = cloneDeep(state);
			state.ShowSignUpInInd = true;
			state.ShowSignInInd = false;
			return state;

		case INTERFACE_SIGNINUSER:
			state = cloneDeep(state);
			state.ShowSignUpInInd = false;
			state.ShowSignInInd = true;
			return state;

		default:
			return state;
	}
};

/* Actions */
export const ShowSignIn = () => ({
	type: INTERFACE_SIGNINUSER,
});

export const ShowSignUp = () => ({
	type: INTERFACE_SIGNUPUSER,
});

/* Dispatch Action */
export const RedirectToSignUp = () => (dispatch) => {
	dispatch(ShowSignUp());
};

export const RedirectToSignIn = () => (dispatch) => {
	dispatch(ShowSignIn());
};

/* Reducer Exports */
export { RegistrationDisplay };
