import { cloneDeep } from 'lodash';

import { createChat, doPostMessage, checkChatExist, addChatToIdToBothUsers, getMessage,
	getProfileB,
	getUserMessages } from '../API/database';

export const POSTMESSAGE = 'PostMessage';
export const CREATECHAT = 'CreateMessage';
export const GETCHATMESSAGES = 'GetChatMessages';
export const ADDUSERPROFILECHAT = 'AddUserProfileChat';
export const POSTERROR = 'ERROR';

const INITIAL_STATE = {
	ChatID: null,
	ChatMessage: null,
	UserMessages: [],
};

function MessageReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case CREATECHAT:
			state = cloneDeep(state);
			state.ChatID = action.payload;
			return state;
		case GETCHATMESSAGES:
			state = cloneDeep(state);
			state.ChatMessage = action.payload;
			return state;
		case ADDUSERPROFILECHAT:
			state = cloneDeep(state);
			state.UserMessages.push(action.payload);
			return state;
		case POSTERROR:
			state = cloneDeep(state);
			state.authUser = action.payload;
			return state;
		default: return state;
	}
}


/* Actions */
export const CreateChatAction = chatID => ({
	type: CREATECHAT,
	payload: chatID,
});
export const GetChatMessagesReducers = chatID => ({
	type: GETCHATMESSAGES,
	payload: chatID,
});
export const errorPost = Error => ({
	type: POSTERROR,
	payload: Error,
});
export const GetProfileUser = PROFILE => ({
	type: ADDUSERPROFILECHAT,
	payload: PROFILE,
});

/* Thunk functions which make API Calls and Action Dispatch */
export const CreateChat = (User1, User2, userFirst) => (dispatch) => {
	let CheckThisUser = User1;
	let NotCheckUser =	User2;
	if (userFirst) {
		CheckThisUser = User2;
		NotCheckUser = User1;
	}

	checkChatExist(NotCheckUser, CheckThisUser)
		.then((snapshot) => {
			if (snapshot.val()) {
				dispatch(CreateChatAction(snapshot.val().chat));
			} else {
				createChat(User1, User2)
					.then((snapshotMessage) => {
						dispatch(CreateChatAction(snapshotMessage.key));
						addChatToIdToBothUsers(snapshotMessage.key, NotCheckUser, CheckThisUser)
							.then(() => {
							})
							.catch((error) => {
								dispatch(errorPost(error));
							});
						addChatToIdToBothUsers(snapshotMessage.key, CheckThisUser, NotCheckUser)
							.then(() => {
							})
							.catch((error) => {
								dispatch(errorPost(error));
							});
					})
					.catch((error) => {
						dispatch(errorPost(error));
					});
			}
		})
		.catch((error) => {
			dispatch(errorPost(error));
		});
};

export const SubmitMessage = (id, messageText, from, to) => (dispatch) => {
	doPostMessage(id, messageText, from, to)
		.then(() => {

		})
		.catch((error) => {
			dispatch(errorPost(error));
		});
};

export const GetChatMessages = id => (dispatch) => {
	getMessage(id)
		.then((snapshot) => {
			dispatch(GetChatMessagesReducers(snapshot.val()));
		})
		.catch((error) => {
			dispatch(errorPost(error));
		});
};

export const GetUserMessages = id => (dispatch) => {
	let keys = [];
	getUserMessages(id)
		.then((snapshot) => {
			keys = Object.keys(snapshot.val());
			for (let i = 0; i < keys.length - 1; i + 1) {
				getProfileB(keys[i])
					.then((snapshotuSER) => {
						dispatch(GetProfileUser(snapshotuSER.val()));
					})
					.catch((error) => {
						dispatch(errorPost(error));
					});
			}
		})
		.catch((error) => {
			dispatch(errorPost(error));
		});
};

export { MessageReducer };
