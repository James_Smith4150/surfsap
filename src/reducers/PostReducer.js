import { cloneDeep } from 'lodash';

import { doSubmitpost, getPosts, LikePost, doCommentpost } from '../API/database';

export const SUBMITPOST = 'SubmitPost';
export const GETPOST = 'GetPost';
export const LIKEPOST = 'LikePost'
export const POSTERROR = 'ERROR';

const INITIAL_STATE = {
	PostText: null,
	Userpost: {},
};

function PostReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case SUBMITPOST:
			state = cloneDeep(state);
			// state.authUser = action.authUser;
			return state;
		case GETPOST:
			state = cloneDeep(state);
			state.Userpost = action.payload;
			return state;
		case POSTERROR:
			state = cloneDeep(state);
			state.authUser = action.payload;
			return state;
		case LIKEPOST:
			state = cloneDeep(state);
			state.authUser = action.payload;
			return state;
		default: return state;
	}
}


/* Actions */
export const setPostList = Post => ({
	type: SUBMITPOST,
	payload: Post,
});
export const errorPost = Error => ({
	type: POSTERROR,
	payload: Error,
});
export const getUserPost = PostList => ({
	type: GETPOST,
	payload: PostList,
});

export const LIKE = like => ({
	type: GETPOST,
	payload: like,
});


/* Thunk functions which make API Calls and Action Dispatch */

export const GetSubmissionsPost = () => (dispatch) => {
	getPosts()
		.then(((snapshot) => {
			dispatch(getUserPost(snapshot.val()));
		}))
		.catch((error) => {
			dispatch(errorPost(error));
		});
};

export const submitPost = (PostText, UserID, displayName) => (dispatch) => {
	doSubmitpost(PostText, UserID, displayName)
		.then((() => {
			getPosts()
				.then(((snapshot) => {
					dispatch(getUserPost(snapshot.val()));
				}))
				.catch((error) => {
					dispatch(errorPost(error));
				});
		}))
		.catch((error) => {
			dispatch(errorPost(error));
		});
};

export const likePost = (id, likes) => (dispatch) => {
	LikePost(id, likes)
		.then((() => {
			getPosts()
				.then(((snapshot) => {
					dispatch(getUserPost(snapshot.val()));
				}))
				.catch((error) => {
					dispatch(errorPost(error));
				});
		}))
		.catch((error) => {
			dispatch(errorPost(error));
		});
};

export const commentPost = (postID, commentText, userid, username)=> (dispatch) =>{
	doCommentpost(postID, commentText, userid, username)
		.then((() => {
			getPosts()
				.then(((snapshot) => {
					dispatch(getUserPost(snapshot.val()));
				}))
				.catch((error) => {
					dispatch(errorPost(error));
				});
		}))
		.catch((error) => {
			dispatch(errorPost(error));
		});
};


export { PostReducer };
