import { cloneDeep } from 'lodash';
import { getProfile, doManageUser, getAuthDetails } from '../API/database';
import { doCurrentUserUpdate, doPasswordUpdate } from '../API/authenthication';
// import { auth } from '../API/firebase';


export const GET_USERPROFILE = 'Get_UserProfile';
export const USERUPDATE_FAIL = 'UserUpdate_fail';
export const GET_USERAUTHDETAILS = 'Get_UserAuthDetails';

// Manage User Profile Details Initial State
const ProfileInitialState = {
	firstName: '',
	lastName: '',
	age: '',
	country: '',
};


/* Reducers */
const HandleUserProfile = (state = ProfileInitialState, action) => {
	switch (action.type) {
		case GET_USERPROFILE:
			state = cloneDeep(state);
			state.UserProfile = action.payload;
			return state;
		case GET_USERAUTHDETAILS:
			state = cloneDeep(state);
			state.UserAuth = action.payload;
			return state;
		case USERUPDATE_FAIL:
			state = cloneDeep(state);
			state.error = action.payload.error;
			return state;

		default:
			return state;
	}
};

/* Actions */
export const SetUserProfile = UserProfile => ({
	type: GET_USERPROFILE,
	payload: UserProfile,
});

export const SetUserAuthDetails = UserAuthDetails => ({
	type: GET_USERAUTHDETAILS,
	payload: UserAuthDetails,
});

export const UserProfileUpdateFailed = error => ({
	type: USERUPDATE_FAIL,
	payload: error,
});

/* Thunk functions which make API Calls and Action Dispatch */
export const getUserProfile = uid => (dispatch) => {
	if (uid) {
		getProfile(uid)
			.then(((snapshot) => {
				dispatch(SetUserProfile(snapshot.val()));
			}))
			.catch((error) => {
				dispatch(UserProfileUpdateFailed(error));
			});
	}
};

export const doUserUpdate = (uid, firstName, lastName, age, country, username, email) =>
	(dispatch) => {
		doManageUser(uid, firstName, lastName, age, country)
			.then(((snapshot) => {
				dispatch(SetUserProfile(snapshot.val()));
			}))
			.catch((error) => {
				dispatch(UserProfileUpdateFailed(error));
			});
		doCurrentUserUpdate(username, email)
			.then((() => {

			})).catch((error) => {
				dispatch(UserProfileUpdateFailed(error));
			});
		// doPasswordUpdate(Password)
		// 	.then((() => {
		// 	})).catch((error) => {
		// 		dispatch(UserProfileUpdateFailed(error));
		// 	});
		// getProfile(uid)
		// 	.then(((snapshot) => {
		// 		dispatch(SetUserProfile(snapshot.val()));
		// 	}))
		// 	.catch((error) => {
		// 		dispatch(UserProfileUpdateFailed(error));
		// 	});
	};

export const getAuthUserDeatils = uid => (dispatch) => {
	getAuthDetails(uid).then(((snapshot) => {
		dispatch(SetUserAuthDetails(snapshot));
	}))
		.catch((error) => {
			dispatch(UserProfileUpdateFailed(error));
		});
};

/* Reducer Exports */
export { HandleUserProfile };
