import { reducer as formReducer } from 'redux-form';
import { combineReducers } from 'redux';
import { UserAuthethication } from './AuthenticationReducer';
import { RegistrationDisplay } from './InterfaceReducer';
import { HandleUserProfile } from './ProfileReducer';
import { PostReducer } from './PostReducer';
import SessionReducer from './SessionReducer';
import BeachReducer from './BeachReducer';
import { MessageReducer } from './MessageReducer';

const rootReducer = combineReducers({
	form: formReducer,
	auth: UserAuthethication,
	profile: HandleUserProfile,
	Interface: RegistrationDisplay,
	sessionState: SessionReducer,
	BeachList: BeachReducer,
	SubmitPost: PostReducer,
	Message: MessageReducer,
});

export default rootReducer;
