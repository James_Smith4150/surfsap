import { cloneDeep } from 'lodash';

const INITIAL_STATE = {
	authUser: null,
};

function SessionReducer(state = INITIAL_STATE, action) {
	switch (action.type) {
		case 'AUTH_USER_SET':
			state = cloneDeep(state);
			state.authUser = action.authUser;
			return state;
		default: return state;
	}
}

export default SessionReducer;
