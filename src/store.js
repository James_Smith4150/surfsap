import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/RootReducer'; // Combines all our reducers

const store = createStore(rootReducer, applyMiddleware(thunk));
export default store;
