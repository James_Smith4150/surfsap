/*eslint-disable */
export function ConvertDate(dateDB) {
  const date2 = new Date();
  const date1 = new Date(dateDB);
  const timeDiff = Math.abs(date2.getTime() - date1.getTime());
  let dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1;
  let date = '';
  if (dayDifference <= 7){
    if(dayDifference <=  1)
     date = `${dayDifference} day`;
    else date = `${dayDifference} days`;
   }
  else {
    dayDifference %= 7;
    date = `${dayDifference} weeks`;
  }
  return date;
}

export function checkMessages(to, currentUser) {
  if (to.localeCompare(currentUser) === 0){
    return true;
  } else {
    return false
  }
}
/* eslint-enable */
