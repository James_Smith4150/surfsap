/**
 * These are validation functions that are used in forms
 */

export const validateRequired = value => (value ? undefined : 'Required');
const validateMinLength = min => value => ((value && value.length) < min ? `Must be ${min} characters or more` : undefined);
export const validateMinLength5 = validateMinLength(5);
export const validateEmail = value => (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? 'Invalid email address' : undefined);
export const validateUserName = value => (validateMinLength(8) && value && !/^[0-9a-zA-Z_.-]+$/.test(value) ? 'Username accepts numbers, letters and (_, . , -)' : undefined);
export const sameAsOriginal = value => (value ? undefined : 'Required');
